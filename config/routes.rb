Rails.application.routes.draw do
# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'

  resources :digs
  resources :assists
  resources :blocks
  resources :passes
  resources :serves
  resources :hits

  resources :games do
    get :increaseHomeScore
    get :decreaseHomeScore
    get :increaseAwayScore
    get :decreaseAwayScore
  end

  resources :users


  resources :teams

  resources :players





end
