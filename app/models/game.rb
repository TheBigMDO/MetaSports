class Game < ApplicationRecord
  has_many :teams, foreign_key: "home_team"
  has_many :teams, foreign_key: "away_team"

  def homeTeam
    Team.find(self.home_team)
  end

  def awayTeam
    Team.find(self.away_team)
  end

  def homeTeamName
    team = Team.find(self.home_team)
    team.name
  end

  def awayTeamName
    team = Team.find(self.away_team)
    team.name
  end

  def name
    "#{self.homeTeamName} vs. #{self.awayTeamName}"
  end
end
