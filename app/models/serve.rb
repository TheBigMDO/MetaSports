class Serve < ApplicationRecord
  has_many :players, foreign_key: "player"
  has_many :games, foreign_key: "game"

  def player_name
    player = Player.find(self.player)
    player.name
  end

  def game_date
    game = Game.find(self.game)
    game.created_at
  end

  def type_of_serve
    if self.level == 1
      "Error"
    elsif self.level == 2
      "Attempt"
    else
      "Ace"
    end
  end
end
