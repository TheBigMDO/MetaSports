class Player < ApplicationRecord
  has_one :teams, foreign_key: "team"

  def team_name
    team = Team.find(self.team)
    team.name
  end

  def games
    team = Team.find(self.team)
    games = Game.where(home_team: team).or(Game.where(away_team: team)).count
  end
end
