class BlocksController < ApplicationController
  def new
    @block = Block.new
  end

  def index
    @blocks = Block.all
  end

  def create
    @block = Block.new(block_params)
    @block.save
  end




  private

  def block_params
    params.require(:block).permit(:player, :game)
  end
end
