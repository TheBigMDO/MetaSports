class PassesController < ApplicationController
  def new
    @pass = Pass.new
  end

  def index
    @passes = Pass.all
  end

  def create
    @pass = Pass.new(pass_params)
    @pass.save
  end




  private

  def pass_params
    params.require(:pass).permit(:player, :game, :level)
  end
end
