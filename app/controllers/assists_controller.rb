class AssistsController < ApplicationController
  def new
    @assist = Assist.new
  end

  def index
    @assists = Assist.all
  end

  def create
    @assist = Assist.new(assist_params)
    @assist.save
  end




  private

  def assist_params
    params.require(:assist).permit(:player, :game)
  end
end
