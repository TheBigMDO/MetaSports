class HitsController < ApplicationController
  def new
    @hit = Hit.new
  end

  def index
    @hits = Hit.all
  end

  def create
    @hit = Hit.new(hit_params)
    @hit.save
  end




  private

  def hit_params
    params.require(:hit).permit(:player, :game, :level)
  end
end
