class GamesController < ApplicationController
  def new
    # @game = Game.new
  end

  def create
    @game = Game.new(game_params)
    if @game.save
      redirect_to @game
    else
      render "new"
    end
  end

  def index
    @games = Game.all
  end

  def show
    @game = Game.find(params[:id])
  end

  def destroy
    @game = Game.find(params[:id])
    @game.destroy
    redirect_to '/games', :notice => "The game was deleted."
  end

  def increaseHomeScore
    @game = Game.find(params[:game_id])
    @game.increment! :home_score
    redirect_to @game
  end

  def increaseAwayScore
    @game = Game.find(params[:game_id])
    @game.increment! :away_score
    redirect_to @game
  end

  def decreaseHomeScore
    @game = Game.find(params[:game_id])
    @game.decrement! :home_score
    redirect_to @game
  end

  def decreaseAwayScore
    @game = Game.find(params[:game_id])
    @game.decrement! :away_score
    redirect_to @game
  end

  private

  def game_params
    params.require(:game).permit(:home_team, :away_team, :home_score, :away_score)
  end

end
