class PlayersController < ApplicationController

  def create
    @player = Player.new(player_params)
    if @player.save
      redirect_to Team.find(@player.team)
    else
      render 'new'
    end
  end

  def index
    @players = Player.all
  end

  def show
    @player = Player.find(params[:id])
  end

  def destroy
    @player = Player.find(params[:id])
    @player.destroy
    redirect_to '/players', :notice => "The game was deleted."
  end

  private

  def player_params
    params.require(:player).permit(:name, :age, :height, :team)
  end
end
