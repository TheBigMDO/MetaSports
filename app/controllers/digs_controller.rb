class DigsController < ApplicationController
  def new
    @dig = Dig.new
  end

  def index
    @digs = Dig.all
  end

  def create
    @dig = Dig.new(dig_params)
    @dig.save
  end




  private

  def dig_params
    params.require(:dig).permit(:player, :game)
  end
end
