class CreateAssists < ActiveRecord::Migration[5.0]
  def change
    create_table :assists do |t|
      t.integer :player
      t.integer :game

      t.timestamps
    end
  end
end
