class CreateHits < ActiveRecord::Migration[5.0]
  def change
    create_table :hits do |t|
      t.integer :player
      t.integer :game
      t.integer :level
    end
  end
end
