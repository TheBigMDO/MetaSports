class UpdateServes < ActiveRecord::Migration[5.0]
  def change
    change_table :serves do |t|
      t.remove :type
      t.integer :level
    end
  end
end
