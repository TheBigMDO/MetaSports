class UpdateHits < ActiveRecord::Migration[5.0]
  def change
    change_table :hits do |t|
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
