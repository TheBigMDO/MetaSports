class BuildUserModel < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :f_name, :string
    add_column :users, :l_name, :string
    add_column :users, :last_login, :date
    add_column :users, :date_created, :date
  end
end
