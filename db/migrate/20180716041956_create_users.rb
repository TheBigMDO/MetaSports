class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :title
      t.integer :team
      t.integer :admin

      t.timestamps
    end
  end
end
